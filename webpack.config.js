const path = require('path');
const webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
  mode: 'production',
  // базовый путь к проекту
  context: path.resolve(__dirname, 'src'),

  // точка входа js
  entry: {
    main: "./index.js" // файл, в котором подключаются все модули
  },

  // точка выхода
  output: {
    path: path.resolve(__dirname, 'dist'), // папка, куда Webpack будет размещать готовый файл
    filename: 'js/[name].js', // файл, куда будут собиратся все модули
    // publicPath: 'dist'    
  },
  devServer: {
    // contentBase: path.join(__dirname, 'dist'),
    overlay: true    
  },
  module: {
    rules: [
      // JS
      {
        test: /\.js$/, // // проверка, является ли файл js-файлом (с раширением js)
        loader: 'babel-loader', // обработчик, который будет обрабатывать js файлы
        exclude: '/node_modules/' // исключить файлы из обработки
      },  
      // SCSS, SASS, CSS
      {
        test: /\.(sass|scss|css)$/,
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {              
              sourceMap: true
            }
          }, {
            loader: 'postcss-loader',
            options: {            
              sourceMap: true,
              config: {
                path: 'postcss.config.js'
              }
            }
          }, {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      // IMAGE
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              publicPath: '../',
              useRelativePath: true
            },
          },
          {
            loader: 'image-webpack-loader',
            options: {              
              mozjpeg: {
                progressive: true,
                quality: 70
              },
              optipng: {
                enabled: false,
              },
              pngquant: {
                quality: '65-90',
                speed: 4
              },
              gifsicle: {
                interlaced: false
              },              
              webp: {
                quality: 75
              }
            }
          }          
        ]
      },
      {
        test: /\.(woff|woff2|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              publicPath: '../',
            },
          }
        ]
      }
    ]
  },

  // плагины
  plugins: [    
    new CleanWebpackPlugin(['./dist']), // очищаем папку dist
    new HtmlWebpackPlugin({
      template: './index.html', // путь к шаблону
      filename: 'index.html', // файл для записи (можно указать подкаталог (например, assets/admin.html)
      inject: true, // true или 'body' все ресурсы js будут размещены внизу элемента body. 'head' поместит скрипты в элемент head      
    }),
    new MiniCssExtractPlugin({
      filename: "css/[name].css", // файл в папке dist, куда будут собираться все стили       
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    })
  ]
}